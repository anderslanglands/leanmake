#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <OpenEXR/ImfRgbaFile.h>
#include <OpenEXR/ImfArray.h>

#define USE_OIIO

#ifdef USE_OIIO
#include <OpenImageIO/imageio.h>
OIIO_NAMESPACE_USING
#endif

// integer modulo function
int mod(int a, int b)
{
    int r = a % b;
    return r < 0 ? r + b : r;
}

void
createLEAN(const char fileIn[], const char fileOutB[], const char fileOutM[])
{
#ifdef USE_OIIO

	ImageInput* in = ImageInput::open(fileIn);
	if (!in) return;
	const ImageSpec& spec = in->spec();
	int width = spec.width;
	int height = spec.height;
	int nc = spec.nchannels;
	std::vector<float> pixels(width*height*nc);
	in->read_image(TypeDesc::FLOAT, &pixels[0]);
	in->close();
	delete in;

	Imf::Array2D<Imf::Rgba> pixelsIn (10, width);
	Imf::Array2D<Imf::Rgba> pixelsB (10, width);
	Imf::Array2D<Imf::Rgba> pixelsM (10, width);

	pixelsIn.resizeErase (height, width);
	pixelsB.resizeErase (height, width);
	pixelsM.resizeErase (height, width);
	std::cout << "height: " << height << " width: " << width << std::endl;

	for (int y=0; y < height; ++y)
	{
		for (int x=0; x < width; ++x)
		{
			pixelsIn[x][y].r = pixels[(y*width+x)*nc];
		}
	}

#else
	Imf::RgbaInputFile file (fileIn);
	Imath::Box2i dw = file.dataWindow();

	int width  = dw.max.x - dw.min.x + 1;
	int height = dw.max.y - dw.min.y + 1;
	Imf::Array2D<Imf::Rgba> pixelsIn (10, width);
	Imf::Array2D<Imf::Rgba> pixelsB (10, width);
	Imf::Array2D<Imf::Rgba> pixelsM (10, width);

	pixelsIn.resizeErase (height, width);
	pixelsB.resizeErase (height, width);
	pixelsM.resizeErase (height, width);
	std::cout << "height: " << height << " width: " << width << std::endl;

	file.setFrameBuffer (&pixelsIn[0][0] - dw.min.x - dw.min.y * width, 1, width);
	file.readPixels (dw.min.y, dw.max.y);
#endif	
	double dbdu, dbdv, prevU, nextU, prevV, nextV;
	Imf::Rgba *M;
	Imf::Rgba *B;
	double invWidth = 1./width;
	double invHeight = 1./height;
	for(int x=0; x<width; ++x){
		for (int y=0; y<height; ++y){
			nextU = pixelsIn[mod(x-1, width-1)][y].r;
			prevU = pixelsIn[mod(x+1, width-1)][y].r;
			dbdu = (nextU/2.)/invWidth - (prevU/2.) / invWidth;

			prevV = pixelsIn[x][mod(y-1, height-1)].r;
			nextV = pixelsIn[x][mod(y+1, height-1)].r;
			dbdv = (nextV/2.)/invHeight - (prevV/2.)/invHeight;

			B = &pixelsB[x][y];
			B->r = dbdv*0.01f;
			B->g = dbdu*0.01f;

			M = &pixelsM[x][y];
			M->r = B->r*B->r;
			M->g = B->g*B->g;
			M->b = B->r*B->g;
		}
	}

	Imf::RgbaOutputFile fileB(fileOutB, width, height, Imf::WRITE_RGBA);
	fileB.setFrameBuffer (&pixelsB[0][0], 1, width);
	fileB.writePixels (height);

	Imf::RgbaOutputFile fileM(fileOutM, width, height, Imf::WRITE_RGBA);
	fileM.setFrameBuffer (&pixelsM[0][0], 1, width);
	fileM.writePixels (height);

}


int main (int argc, char *argv[])
{
 	 if (argc != 4)
    {
    	fprintf(stdout,"Usage: %s\n filein fileout_B fileout_M\n",argv[0]);
    	return 1;
    }
	
	std::cout << "Reading image" << std::endl;
	createLEAN(argv[1], argv[2], argv[3]);
	std::cout << "Completed writing image" << std::endl;
	
  	return 0;
}

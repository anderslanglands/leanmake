cmake_minimum_required (VERSION 2.6)

project (leanmake)

set(CMAKE_BUILD_TYPE DEBUG)

set(ILMBASE_ROOT /Users/anders/vfx/ilmbase/2.1.0)
set(OPENEXR_ROOT /Users/anders/vfx/ilmbase/2.1.0)
set(OIIO_ROOT /Users/anders/vfx/oiio/1.5.0)

include_directories(${ILMBASE_ROOT}/include)
link_directories(${ILMBASE_ROOT}/lib)

include_directories(${OPENEXR_ROOT}/include)
link_directories(${OPENEXR_ROOT}/lib)

include_directories(${OIIO_ROOT}/include)
link_directories(${OIIO_ROOT}/lib)

find_library(IMFLIB
			NAMES IlmImf
			PATHS ${OPENEXR_ROOT}/lib)
find_library(HALFLIB 
			NAMES Half
			PATHS ${ILMBASE_ROOT}/lib)
find_library(OIIOLIB 
			NAMES OpenImageIO
			PATHS ${OIIO_ROOT}/lib)

add_executable(leanmake leanmake.cpp)
target_link_libraries(leanmake ${IMFLIB} ${HALFLIB} ${OIIOLIB})
